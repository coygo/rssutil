#!/bin/sh
# vim: ts=4 sts=4 sw=4 et
#
# Copyright 2018 coy <https://gitlab.com/coygo>. All rights reserved.
# Use of this source code is governed by a GPLv3
# license that can be found in the LICENSE file.

set -e

CWD="$(dirname $(realpath $0))"

. ${CWD}/init_colors.inc.sh

check_gpgsign() {
    # "|| true" means force success
    nogpgsign=$(git config --get --type=bool hooks.nogpgsign || true)
    gpgsignkey=$(git config --get user.signingKey || true)
    commitgpgsign=$(git config --get --type=bool commit.gpgsign || true)

    if ([ "$commitgpgsign" != "true" ] || [ -z $gpgsignkey ]) && [ "$nogpgsign" != "true" ]
    then
        echo
        echo ${RED} ERROR ${RESET}
        echo
        echo You must sign the commit with a GPG Key. You can set your git repo to sign automatically by:
        echo
        echo "    git config commit.gpgsign true"
        echo "    git config user.signingKey <your-signing-key>"
        echo
        echo See https://gitlab.com/help/user/project/repository/gpg_signed_commits/index.md
        echo
        echo If you know what you are doing you can disable this check using:
        echo
        echo "    git config hooks.nogpgsign true"
        echo

        exit 1
    fi
}

check_gpgsign
